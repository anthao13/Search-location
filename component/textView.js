import React, { Component } from 'react';
import { Text, View } from 'react-native';


export default class TextViewS extends Component {
   
  render() {
    return (
        <Text style={{fontSize:16,paddingVertical: 20,borderBottomColor: '#ccc',borderBottomWidth: 1}}>{this.props.location}</Text>
        
    );
  }
}

